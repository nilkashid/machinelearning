from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import pandas as pd
import numpy as np
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier


negative_review_df = pd.read_csv('negative-data.csv')
positive_review_df = pd.read_csv('positive-data.csv')

def create_word_features(words):
    useful_words = [word for word in words if word not in stopwords.words("english")]
    my_dict = dict([(word, True) for word in useful_words])
    return my_dict


def create_positive_review_bag_of_words():
    reviews = []
    for index, row in negative_review_df.iterrows():
        words = nltk.word_tokenize(row['review'])
        reviews.append((create_word_features(words), "negative"))
    return reviews    
        
        
def create_negative_review_bag_of_words():    
    reviews = [] 
    for index, row in positive_review_df.iterrows():
        words = nltk.word_tokenize(row['review'])
        reviews.append((create_word_features(words), "positive"))
    return reviews    
    
        
neg_reviews = create_negative_review_bag_of_words()
pos_reviews = create_positive_review_bag_of_words()    


train_set = neg_reviews[:400] + pos_reviews[:400]
test_set =  neg_reviews[400:] + pos_reviews[400:]
print(len(train_set),  len(test_set))   

classifier = NaiveBayesClassifier.train(train_set)
accuracy = nltk.classify.util.accuracy(classifier, test_set)
print(accuracy * 100)


review_sample = '''
An overhyped place in south Mumbai which has lost its taste and have been charging way over the quantity and quality of the food served. Still a good place to fulfil your midnight cravings if you are in the area.
'''
words = nltk.word_tokenize(review_sample)
words = create_word_features(words)
val = classifier.classify(words)
print(val)

review_sample = '''
I order from here almost every day the same salad and still wonder how can a salad taste so good! (Same salad everyday and still not bored) 
What i tried :
Hercules salad (10/10) :
This salad is oozing with perfection and top-notch quality! Perfect amount of chicken and bacon with a generous amount of greens makes it a great meal afted Gym! 
For sure a must try if you love a meaty salad and Its priced very reasonably for the quality they provide!
'''
words = nltk.word_tokenize(review_sample)
words = create_word_features(words)
val = classifier.classify(words)
print(val)

review_sample = '''
Box 8 has around 30 outlets in the city and hence it is accessible from any corner. They serve and cover almost every part of the city unlike other delivery joints which cater to only specific locations. So, a big 👍👍

The delivery for my order was before time and the packaging was excellent in oil proof containers. The food was warm and super tasty. The quality standards was found to be hygienic and the quantity too was ample. Additionally, they provide with a lot of tissues and spoons. 

Coming to the food, I personally found their Wraps and Sandwiches section interesting enough as it had a variety of options ( Veg and Non Veg ). I tried the following: 
1. Chicken Grilled Sandwich
2. Tandoori Chicken Sandwich
3. Chicken triple decker sandwich
4. Chicken Mayo Wrap 🌯
All the above order items were absolutely amazing and especially the Mayo Wrap was full of Mayonnaise and it was really amazing. 

The Butter Chicken Meal had good Butter Chicken gravy of thick consistency alongwith rice and Parathas. Additionally, Chole and Brownie were found to be tasty. 

The Chicken Extreme and Roast Chicken Curry Fusion Boxes had good Chicken meat pieces which were evenly cooked. The salads and the long Rice grain were fantastic. 

As for Desserts, The chocolate brownie was good. Lemonade was also fine. 

Overall, Box 8 is a perfect combo of super tasty food and Pocket Friendly costs. You can definitely try them out once.'''
words = nltk.word_tokenize(review_sample)
words = create_word_features(words)
val = classifier.classify(words)
print(val)



review_sample = '''
Decided to celebrate my birthday eve here, amidst the captivating Sea Link, the waters of the Arabian Sea and with a special someone. The cover charge per person is 2.5k but when you look at the whole package, its not as expensive as it sounds. The location is ideal date material, but thats quite obvious I guess. The other part that I dint expect to be great, but was quiiiiittteeee nice, was the food. We went ahead with the recommendations of the chef which included asparagus cream cheese rolls, chilly mushrooms and potato and lotus stem fritters. I'm not a fan of mushrooms but I swear I wish they had home delivery: D I would order atleast once a month! The cream cheese rolls were served much like spring rolls except with the cheesy twist and came alongside a chilly dip. Much wow! Wouldnt recommend the fritters though. They were also sweet enough to bring a blueberry cheesecake at 12 for my birthday celebration :D 

The drinks menu is standard ranging from scotch to single malts, 4 types of gin and the vodka tequila regulars. The drinks were strong. Or maybe I'm low on capacity ;) 

Overall the experience was brilliant and I'd recommend it to any couple for a special occasion!
'''
words = nltk.word_tokenize(review_sample)
words = create_word_features(words)
val = classifier.classify(words)
print(val)



    

