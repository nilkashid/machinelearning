import requests

url = "https://i.pinimg.com/originals/2a/05/ce/2a05ce9570eccdb299cb18e7f3c83e7c.jpg"
filename = url.split("/")[-1]
r = requests.get(url, timeout=0.5)

if r.status_code == 200:
    with open('image_file', 'wb') as f:
        f.write(r.content)
