from PIL import Image
from pytesseract import image_to_string
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import pandas as pd
import numpy as np

#print image_to_string(Image.open('test.png'))
textRepost = image_to_string(Image.open('report3.jpg'), lang='eng')
print(textRepost)
#tokenizer = RegexpTokenizer(r'\w+')
tokenizer = RegexpTokenizer('[a-zA-Z]\w+')

wordTokens = tokenizer.tokenize(textRepost)   
english_stops = set(stopwords.words('english'))
simpleReportTokens = [word for word in wordTokens if word not in english_stops]
simpleReportTokens = set([word.lower() for word in wordTokens])
#print(simpleReportTokens)

#print (textRepost)
#print(simpleReportTokens)
#print(len(simpleReportTokens))

df = pd.read_csv('medicaltems.csv')
termsArray = df["medical_terms"].unique()

foundItems = [medicalterm for medicalterm in simpleReportTokens if medicalterm in termsArray]
print()
print("_________ [ TAGS ]_______________________________________________________________________________", end = '\n')
print(sorted(foundItems))
print(len(foundItems))


