from PIL import Image
from pytesseract import image_to_string
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import pandas as pd
import numpy as np

#print image_to_string(Image.open('test.png'))
textRepost = image_to_string(Image.open('report4.jpg'), lang='eng')
#print(textRepost)
tokenizer = RegexpTokenizer('[a-zA-Z]\w+')

wordTokens = tokenizer.tokenize(textRepost)  
wordTokens = set([word.lower() for word in wordTokens])
 
#print(wordTokens)

df = pd.read_csv('medicaltems.csv')
termsArray = df["medical_terms"].unique()

foundItems = [medicalterm for medicalterm in wordTokens if medicalterm in termsArray]
print()
print("_________ [ TAGS ]_______________________________________________________________________________", end = '\n')
print(foundItems)
print(len(foundItems))

# CTAKES


